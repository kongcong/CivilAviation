package com.xj.sale;

import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.ml.regression.LinearRegression;
import org.apache.spark.ml.regression.LinearRegressionModel;
import org.apache.spark.ml.regression.LinearRegressionTrainingSummary;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;

import scala.Tuple2;


public class SaleAnalyze {

    public static void main(String[] args) {

        SparkConf conf = new SparkConf().setAppName("AgentPortrait").setMaster("local");

        JavaSparkContext sc = new JavaSparkContext(conf);
        JavaRDD<String> textFileRDD = sc.textFile("E:\\QQdata\\1420599051\\FileRecv\\1_20170317160335_u9yyy\\sales_sample_20170310.csv");
        // 行数
//		long count = textFileRDD.count();
//		System.out.println("count:" + count);

        // 计算代理人的每天总销售额
//        JavaPairRDD<String,Integer> agentRoundCountPerDay = agentRoundCountPerDay(textFileRDD);
//        agentRoundCountPerDay.persist(StorageLevel.MEMORY_AND_DISK_SER());

        // 计算代理人每天销售额占比
//        List<Tuple2<String,Double>> proportionRoundCountPerDay = proportionRoundCountPerDay(textFileRDD, agentRoundCountPerDay);
//        for (Tuple2<String, Double> tuple2 : proportionRoundCountPerDay) {
//            System.out.println("date_id:" + tuple2._1 + "\tproportion:" + tuple2._2);
//        }

        // 计算代理人的成交量
        //agentcntCountPerDay(textFileRDD);

        // 计算代理人每天的成交量占比


        // 计算pr值

        // 预测销售数据
        predict(textFileRDD, sc);


    }

    private static void predict(JavaRDD<String> textFileRDD, JavaSparkContext sc) {

        SparkConf conf = new SparkConf().setAppName("AgentPortrait").setMaster("local");
        //JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);

        JavaRDD<String[]> map = textFileRDD.map(new Function<String, String[]>() {
            @Override
            public String[] call(String s) throws Exception {
                String[] split = s.split(",");
                return split;
            }
        });
        // 构造数据格式
        JavaRDD<String> map1 = map.map(new Function<String[], String>() {
            @Override
            public String call(String[] strings) throws Exception {
                return strings[4] + " 1:" + strings[0] + " 2:" + strings[3];
            }
        });
        map1.saveAsTextFile("E:\\QQdata\\1420599051\\FileRecv\\1_20170317160335_u9yyy\\data.txt");

        // Load training data
        DataFrame training = sqlContext.read().format("libsvm")
                .load("E:\\QQdata\\1420599051\\FileRecv\\1_20170317160335_u9yyy\\data.txt");

        LinearRegression lr = new LinearRegression()
                .setMaxIter(10)
                .setRegParam(0.3)
                .setElasticNetParam(0.8);

        // Fit the model
        LinearRegressionModel lrModel = lr.fit(training);

        // Print the coefficients and intercept for linear regression
        System.out.println("Coefficients: "
                + lrModel.coefficients() + " Intercept: " + lrModel.intercept());

        // Summarize the model over the training set and print out some metrics
        LinearRegressionTrainingSummary trainingSummary = lrModel.summary();
        System.out.println("numIterations: " + trainingSummary.totalIterations());
        System.out.println("objectiveHistory: " + Vectors.dense(trainingSummary.objectiveHistory()));
        trainingSummary.residuals().show();
        System.out.println("RMSE: " + trainingSummary.rootMeanSquaredError());
        System.out.println("r2: " + trainingSummary.r2());
        // $example off$

        sc.stop();

    }

    /**
     * 计算代理人每天销售额占比
     * @param textFileRDD
     * @return
     */
    private static List<Tuple2<String,Double>> proportionRoundCountPerDay(JavaRDD<String> textFileRDD, JavaPairRDD<String,Integer> agentRoundCountPerDay) {
        // 计算每天的总销售额
        JavaPairRDD<String,Integer> allMoneyPerDay = textFileRDD.filter(new Function<String, Boolean>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Boolean call(String s) throws Exception {
                try {
                    Integer num = Integer.valueOf(s.split(",")[4]);//把字符串强制转换为数字
                    return true;//如果是数字，返回True
                } catch (Exception e) {
                    return false;//如果抛出异常，返回False
                }
            }
        }).mapToPair(new PairFunction<String, String, Integer>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Tuple2<String, Integer> call(String s) throws Exception {
                String[] split = s.split(",");
                String date = split[0];  // 日期
                String saleId = split[1];  // 销售者Id
                String amountMoney = split[4];  //金额
//				System.out.println("amountMoney:" + amountMoney);
                String key = date + "_" + saleId;
                Integer value = Integer.parseInt(amountMoney);
                return new Tuple2<String, Integer>(key, value);
            }
        });
        JavaPairRDD<String, Integer> date2RoundCount = allMoneyPerDay.mapToPair(new PairFunction<Tuple2<String,Integer>, String, Integer>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Tuple2<String, Integer> call(Tuple2<String, Integer> t)
                    throws Exception {
                String date = t._1.split("_")[1];
                return new Tuple2<String, Integer>(date, t._2);
            }
        });

        //按照日期聚合
        JavaPairRDD<String, Integer> resultDate2RoundCount = date2RoundCount.reduceByKey(new Function2<Integer, Integer, Integer>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Integer call(Integer v1, Integer v2) throws Exception {
                return v1 + v2;
            }
        }).reduceByKey(new Function2<Integer, Integer, Integer>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Integer call(Integer v1, Integer v2) throws Exception {
                // TODO Auto-generated method stub
                return v1 + v2;
            }
        });
        // key:date  value：当天的总销售额
        List<Tuple2<String, Integer>> resultDate2CountList = resultDate2RoundCount.collect();

        // 得到所有的日期和出售者拼接的字符串组成的集合
        final List<String> dateAndSaleIds = allMoneyPerDay.map(new Function<Tuple2<String,Integer>, String>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public String call(Tuple2<String, Integer> t) throws Exception {
                return t._1;
            }
        }).collect();

        // key:date_id value:当天销售额
        final List<Tuple2<String,Integer>> allMoneyPerDayList = allMoneyPerDay.collect();

        // 计算代理人每天的销售额占比
        List<Tuple2<String, Double>> agentPerDayProp = agentRoundCountPerDay.mapToPair(new PairFunction<Tuple2<String,Integer>, String, Double>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Tuple2<String, Double> call(Tuple2<String, Integer> tuple)
                    throws Exception {
                Double propRoundCountPerDay = null;
                for (int i = 0; i < allMoneyPerDayList.size(); i++) {
                    String  date_id = allMoneyPerDayList.get(i)._1;
                    // 当日期和代理人编号拼接的字符串相同的时候计算
                    if (date_id.equals(tuple._1)) {
                        propRoundCountPerDay = (double) (tuple._2 / allMoneyPerDayList.get(i)._2);
                    }
                }
                return new Tuple2<String, Double>("date:" + tuple._1, propRoundCountPerDay);
            }
        }).collect();
        return agentPerDayProp;
    }

    /**
     * 计算代理人的成交量
     * @param textFileRDD
     */
    private static void agentcntCountPerDay(JavaRDD<String> textFileRDD) {
        JavaRDD<String> agentRDD = filterAgent(textFileRDD);

        // 代理人每天的销售额
        JavaPairRDD<String,Integer> agentCntPerDay = agentRDD.filter(new Function<String, Boolean>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Boolean call(String s) throws Exception {
                try {
                    Integer num = Integer.valueOf(s.split(",")[3]);//把字符串强制转换为数字
                    return true;//如果是数字，返回True
                } catch (Exception e) {
                    return false;//如果抛出异常，返回False
                }
            }
        }).mapToPair(new PairFunction<String, String, Integer>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Tuple2<String, Integer> call(String s) throws Exception {
                String[] split = s.split(",");
                String date = split[0];  // 日期
                String agentId = split[1];  // 代理人Id
                String cnt = split[3];  // 成交量
//				System.out.println("cnt:" + cnt);
                String key = date + "_" + agentId;
                Integer value = Integer.parseInt(cnt);
                return new Tuple2<String, Integer>(key, value);
            }
        }).reduceByKey(new Function2<Integer, Integer, Integer>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Integer call(Integer v1, Integer v2) throws Exception {
                return v1 + v2;
            }
        });

        agentCntPerDay.foreach(new VoidFunction<Tuple2<String,Integer>>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public void call(Tuple2<String, Integer> tuple) throws Exception {
                System.out.println("date:" + tuple._1 + "\tcnt:" + tuple._2);
            }
        });

    }

    /**
     * 统计代理人每天销售额
     * @param textFileRDD
     * @param sc
     * @return
     */
    private static JavaPairRDD<String,Integer> agentRoundCountPerDay(JavaRDD<String> textFileRDD) {

        JavaRDD<String> agentRDD = filterAgent(textFileRDD);
        // 统计代理人的数据量
        System.out.println("agentRDD: "+agentRDD.count());

        // 代理人每天的销售额
        JavaPairRDD<String,Integer> agentMoneyPerDay = agentRDD.filter(new Function<String, Boolean>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Boolean call(String s) throws Exception {
                try {
                    Integer num = Integer.valueOf(s.split(",")[4]);//把字符串强制转换为数字
                    return true;//如果是数字，返回True
                } catch (Exception e) {
                    return false;//如果抛出异常，返回False
                }
            }
        }).mapToPair(new PairFunction<String, String, Integer>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Tuple2<String, Integer> call(String s) throws Exception {
                String[] split = s.split(",");
                String date = split[0];  // 日期
                String agentId = split[1];  // 代理人Id
                String amountMoney = split[4];  //金额
//				System.out.println("amountMoney:" + amountMoney);
                String key = date + "_" + agentId;
                Integer value = Integer.parseInt(amountMoney);
                return new Tuple2<String, Integer>(key, value);
            }
        }).reduceByKey(new Function2<Integer, Integer, Integer>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Integer call(Integer v1, Integer v2) throws Exception {
                return v1 + v2;
            }
        });
        return agentMoneyPerDay;
//		agentMoneyPerDay.foreach(new VoidFunction<Tuple2<String,Integer>>() {
//
//			/**
//			 *
//			 */
//			private static final long serialVersionUID = 1L;
//
//			@Override
//			public void call(Tuple2<String, Integer> tuple) throws Exception {
//				System.out.println("date:" + tuple._1 + "\tmoney:" + tuple._2);
//			}
//		});


    }

    /**
     * 过滤出Agent的销售数据
     * @param textFileRDD
     * @return
     */
    private static JavaRDD<String> filterAgent(JavaRDD<String> textFileRDD) {
        JavaRDD<String> filterRDD = textFileRDD.filter(new Function<String, Boolean>() {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public Boolean call(String agent) throws Exception {
                return agent.split(",")[1].substring(0, 1).equals("O");
            }
        });
        return filterRDD;
    }

}
